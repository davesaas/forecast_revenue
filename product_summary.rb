require "optparse"
require 'clipboard'
require 'text-table'
require 'money'
require 'i18n'
require "sequel"
require "yaml"

#################
# Add method to array class to convert to format using money
class Array
  def format_money

  end
end
##############
# Start body
#puts "\u2B06"

#options parsing
options = {:copy => false, :format => false}
parser = OptionParser.new do|opts|
	opts.banner = "Usage: product_summary.rb [options], requires copy of SFDC table in clipboard"
	opts.on('-c','--copy', 'Copy output in clipboard') do |str|
		options[:copy] = true;
	end
  opts.on('-f','--format', 'Format output of table') do |str|
		options[:format] = true;
	end
	opts.on('-h', '--help', 'Displays Help') do
		puts opts
		exit
	end
end

parser.parse!


I18n.enforce_available_locales = false
forecast_raw = Clipboard.paste

if !(/Close Date/ =~ forecast_raw)

  puts "Clipboard does not contain forecast data"
  puts "***** clipboard contents *****"
  puts forecast_raw
  exit
end

#create in memory db
db = Sequel.sqlite

db.create_table :forecast_lines do
  primary_key :id
  String :bus_unit
  String :prod_family
  String :rev_type
  String :quota_cat
  String :quota_type
  Float :closed
  Float :commit
  Float :bestcase
  Float :pipeline
end
forecast_lines = db[:forecast_lines]

forecast_cats = []
num_of_cats = 0
bus_unit = ""
prod_family = ""
rev_type = "SYB"

#load YAML in data structures
config_info = YAML.load_file("/Users/dsaas/Stuff/code/forecast_revenue/product_types.yml")
product_types = config_info["product_types"]
family_types = config_info["family_types"]


forecast_raw.each_line do |line|
  segments = line.split("\t")
  if /Primary Business Unit/ =~ line
    forecast_cats.concat  segments[3..-2].map { |e| e.downcase.gsub(/ /,"")  }
    num_of_cats = segments.length - 4
    puts "Number of cats - #{num_of_cats}"
    next
  end

  #skip if a Subtotal line
  next if line =~ /Subtotal/
  next if line =~ /Close Date/
  next if line =~ /Grand Total/

  bus_unit = segments[0] if segments[0].length > 0
  prod_family = segments[1] if segments[1].length > 0


  # puts segments.length
  # puts line
  # segments.each_with_index { |e, i| puts "#{i} - #{e}" }
  # puts segments[0]
  # puts segments[1]
  #puts forecast_cats

  quota_cat = product_types.select{|x| x["product"] == bus_unit}.first["category"]
  quota_type = family_types.select{|x| x["family"] == prod_family}.first["category"]




  forecast_values_tb = {}
  forecast_values_syb = {}
  #Assign float values to forecast categories
  # puts forecast_cats
  segments[3..(2+num_of_cats)].each_with_index do |cat_value,index|
    value_split = cat_value.split(" ")
    # puts "#{cat_value} - #{index}"
    forecast_values_tb[forecast_cats[index].to_sym] = value_split[0].strip.gsub(/[\$\,]/,"").to_f
    forecast_values_syb[forecast_cats[index].to_sym] = value_split[1].strip.gsub(/[\$\,]/,"").to_f
  end
  # puts forecast_values_tb
  # puts forecast_values_syb

  #puts "#{rev_type} - #{bus_unit} - #{prod_family} - #{quota_cat} - #{quota_type} - #{segments[4]}"
  #insert TB amounts
  forecast_lines.insert(
    bus_unit: bus_unit,
    prod_family: prod_family,
    rev_type: "TB",
    quota_cat: quota_cat,
    quota_type: quota_type,
    closed: (forecast_values_tb[:closed] ||= 0.0),
    commit: (forecast_values_tb[:commit] ||= 0.0),
    bestcase: (forecast_values_tb[:bestcase] ||= 0.0),
    pipeline: (forecast_values_tb[:pipeline] ||= 0.0)
  )
  #insert SYB amounts
  forecast_lines.insert(
    bus_unit: bus_unit,
    prod_family: prod_family,
    rev_type: "SYB",
    quota_cat: quota_cat,
    quota_type: quota_type,
    closed: (forecast_values_syb[:closed] ||= 0.0),
    commit: (forecast_values_syb[:commit] ||= 0.0),
    bestcase: (forecast_values_syb[:bestcase] ||= 0.0),
    pipeline: (forecast_values_syb[:pipeline] ||= 0.0)
  )



end

puts "There were #{forecast_lines.count} records\n"

totalTB_hash = Hash.new 0
totalTB = forecast_lines.where(rev_type: "TB").select
totalTB.each do |record|
  totalTB_hash[:closed] += record[:closed]
  totalTB_hash[:commit] += record[:commit]
  totalTB_hash[:bestcase] += record[:bestcase]
  totalTB_hash[:pipeline] += record[:pipeline]
  totalTB_hash[:closed_commit] += record[:closed] + record[:commit]
end

totalSYB_hash = Hash.new 0
totalSYB = forecast_lines.where(rev_type: "SYB").select
totalSYB.each do |record|
  totalSYB_hash[:closed] += record[:closed]
  totalSYB_hash[:commit] += record[:commit]
  totalSYB_hash[:bestcase] += record[:bestcase]
  totalSYB_hash[:pipeline] += record[:pipeline]
  totalSYB_hash[:closed_commit] += record[:closed] + record[:commit]
end

mwTB_hash = Hash.new 0
mwTB = forecast_lines.where(quota_cat: "Middleware").where(quota_type: "Subscriptions").where(rev_type: "TB").select
mwTB.each do |record|
  mwTB_hash[:closed] += record[:closed]
  mwTB_hash[:commit] += record[:commit]
  mwTB_hash[:bestcase] += record[:bestcase]
  mwTB_hash[:pipeline] += record[:pipeline]
  mwTB_hash[:closed_commit] += record[:closed] + record[:commit]
end

mwSYB_hash = Hash.new 0
mwSYB = forecast_lines.where(quota_cat: "Middleware").where(quota_type: "Subscriptions").where(rev_type: "SYB").select
mwSYB.each do |record|
  mwSYB_hash[:closed] += record[:closed]
  mwSYB_hash[:commit] += record[:commit]
  mwSYB_hash[:bestcase] += record[:bestcase]
  mwSYB_hash[:pipeline] += record[:pipeline]
  mwSYB_hash[:closed_commit] += record[:closed] + record[:commit]
end

cldTB_hash = Hash.new 0
cldTB = forecast_lines.where(quota_cat: "Cloud").where(quota_type: "Subscriptions").where(rev_type: "TB").select
cldTB.each do |record|
  cldTB_hash[:closed] += record[:closed]
  cldTB_hash[:commit] += record[:commit]
  cldTB_hash[:bestcase] += record[:bestcase]
  cldTB_hash[:pipeline] += record[:pipeline]
  cldTB_hash[:closed_commit] += record[:closed] + record[:commit]
end

cldSYB_hash = Hash.new 0
cldSYB = forecast_lines.where(quota_cat: "Cloud").where(quota_type: "Subscriptions").where(rev_type: "SYB").select
cldSYB.each do |record|
  cldSYB_hash[:closed] += record[:closed]
  cldSYB_hash[:commit] += record[:commit]
  cldSYB_hash[:bestcase] += record[:bestcase]
  cldSYB_hash[:pipeline] += record[:pipeline]
  cldSYB_hash[:closed_commit] += record[:closed] + record[:commit]
end

servTB_hash = Hash.new 0
servTB = forecast_lines.where(quota_type: "Services").where(rev_type: "TB").select

servTB.each do |record|
  servTB_hash[:closed] += record[:closed]
  servTB_hash[:commit] += record[:commit]
  servTB_hash[:bestcase] += record[:bestcase]
  servTB_hash[:pipeline] += record[:pipeline]
  servTB_hash[:closed_commit] += record[:closed] + record[:commit]
end

servSYB_hash = Hash.new 0
servSYB = forecast_lines.where(quota_type: "Services").where(rev_type: "SYB").select
servSYB.each do |record|
  servSYB_hash[:closed] += record[:closed]
  servSYB_hash[:commit] += record[:commit]
  servSYB_hash[:bestcase] += record[:bestcase]
  servSYB_hash[:pipeline] += record[:pipeline]
  servSYB_hash[:closed_commit] += record[:closed] + record[:commit]
end


#Create table for Output
table = Text::Table.new
table.head = mwTB_hash.keys.unshift("Rev Type").unshift("Adder").map { |e| e.upcase  }
if options[:format]
  table.rows << totalTB_hash.values.map{|x| Money.new(x*100,"USD").format}.unshift("TB").unshift("Total")
  table.rows << totalSYB_hash.values.map{|x| Money.new(x*100,"USD").format}.unshift("SYB").unshift("Total")
  table.rows << mwTB_hash.values.map{|x| Money.new(x*100,"USD").format}.unshift("TB").unshift("Middleware")
  table.rows << mwSYB_hash.values.map{|x| Money.new(x*100,"USD").format}.unshift("SYB").unshift("Middleware")
  table.rows << cldTB_hash.values.map{|x| Money.new(x*100,"USD").format}.unshift("TB").unshift("Cloud")
  table.rows << cldSYB_hash.values.map{|x| Money.new(x*100,"USD").format}.unshift("SYB").unshift("Cloud")
  table.rows << servTB_hash.values.map{|x| Money.new(x*100,"USD").format}.unshift("TB").unshift("Services")
  table.rows << servSYB_hash.values.map{|x| Money.new(x*100,"USD").format}.unshift("SYB").unshift("Services")
else
  table.rows << totalTB_hash.values.unshift("TB").unshift("Total")
  table.rows << totalSYB_hash.values.unshift("SYB").unshift("Total")
  table.rows << mwTB_hash.values.unshift("TB").unshift("Middleware")
  table.rows << mwSYB_hash.values.unshift("SYB").unshift("Middleware")
  table.rows << cldTB_hash.values.unshift("TB").unshift("Cloud")
  table.rows << cldSYB_hash.values.unshift("SYB").unshift("Cloud")
  table.rows << servTB_hash.values.unshift("TB").unshift("Services")
  table.rows << servSYB_hash.values.unshift("SYB").unshift("Services")
end


puts table.to_s

puts "TB\tSYB"

out_str = "#{mwTB_hash[:closed_commit]}\t#{mwSYB_hash[:closed_commit]}\n"
out_str << "#{cldTB_hash[:closed_commit]}\t#{cldSYB_hash[:closed_commit]}\n"
out_str << "#{servTB_hash[:closed_commit]}\t#{servSYB_hash[:closed_commit]}\n"
puts out_str

if options[:copy]
  Clipboard.copy(out_str)
end
