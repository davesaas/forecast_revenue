require 'clipboard'

forecast_raw = Clipboard.paste

forecast_raw.each_line do |line|
  segments = line.split("\t")
  puts "#{segments[0]} - #{segments.length}"
end
